package com.huanghwh.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: huanghwh
 * @Date: 2023/04/09 下午3:26
 * @Description: nacos配置中心使用示例
 */
@RestController
@RefreshScope /*该注解确保配置刷新*/
public class NacosConfigController {

    @Value("${username}")
    private String username;

    @Value("${timeout}")
    private String timeout;

    @GetMapping("/test")
    public String test() {
        return "输出：" + username + "---" + timeout;
    }

}
